import '@babel/polyfill';
import * as microsoftTeams from '@microsoft/teams-js';
import * as csstips from 'csstips';
import { getContext, TeamsThemeContext, ThemeStyle } from 'msteams-ui-components-react';
import * as React from 'react';
import { render } from 'react-dom';

csstips.normalize();
import { IconButton, Panel, PanelBody, Tr } from 'msteams-ui-components-react';
import { MSTeamsIconType } from 'msteams-ui-icons-react';
import axios from 'axios';
//import "./../components/style.css"
import { Checkbox, Table, TBody, Th, Td, THead, PrimaryButton, SecondaryButton } from 'msteams-ui-components-react'
interface IState {
  data: any[any],
  isLoading: boolean,
  isError: boolean,
  isSuccess: boolean,
  type: string,
  replyToId: number,
  isChecked: boolean
}

export class GHPages extends React.Component<{}, IState> {
  state = {
    isLoading: true,
    isError: false,
    isSuccess: false,
    data: [],
    type: "",
    replyToId: 0,
    isChecked: false
  }


  async componentDidMount() {
    let res = await axios.get('https://run.mocky.io/v3/08b33fcc-a45d-4ae3-8454-13278a71879d');
    let replyToId = 26101987;
    if (res.status === 200) {
      let data = [];
      for (let i in res.data) {
        if (res.data[i].details.subCategory === "Mobile") {
          res.data[i].details.subCategoryId = MSTeamsIconType.Mobile;
        }
        else if (res.data[i].details.subCategory === "Laptop") {
          res.data[i].details.subCategoryId = MSTeamsIconType.GVCFocus;
        }
        else if (res.data[i].details.subCategory === "Desktop") {
          res.data[i].details.subCategoryId = MSTeamsIconType.Presentation;
        }
        data.push({ ...res.data[i], isActive: false, isChecked: false });
      }
      this.setState({ data: data, isSuccess: true, isLoading: false, replyToId: replyToId });
    }
    else
      this.setState({ data: null, isError: true, isLoading: false });
  }
  componentWillReceiveProps(props) {

  }
  selectAll = (e) => {
    const { isChecked, data } = { ...this.state };
    if (!isChecked) {
      for (let i in data) {
        data[i].isActive = false;
        data[i].isChecked = true;
      }
    }
    else {
      for (let i in data) {

        data[i].isChecked = false;
      }
    }
    this.setState({ isChecked: !isChecked, data: data });
  }
  onApprove = (index) => {
    const dataObj = this.state.data;
    let selectedData = [];
    if (typeof index === 'undefined') {
      for (let i in dataObj) {
        if (dataObj[i].isChecked)
          selectedData.push(dataObj[i]);
      }
      if (selectedData.length) {
        let customerInfo = {
          pageType: "benefit registrations",
          details: { selectedData, replyToId: this.state.replyToId },
          status: true,
          sendFor: "approve",
          remark: " test remark"
        }
        microsoftTeams.tasks.submitTask(customerInfo, "e0e99be3-0751-4c65-8d14-6c29877f6a2d");
        return true;
      }
    } else {
      selectedData.push(dataObj[index]);
      if (selectedData.length) {
        let customerInfo = {
          pageType: "benefit registrations",
          details: { selectedData, replyToId: this.state.replyToId },
          status: true,
          sendFor: "approve",
          remark: " test remark"
        }
        microsoftTeams.tasks.submitTask(customerInfo, "e0e99be3-0751-4c65-8d14-6c29877f6a2d");
        return true;
      }
    }
  }
  onReject = (index) => {
    const dataObj = this.state.data;
    let selectedData = [];
    if (typeof index === 'undefined') {
      for (let i in dataObj) {
        if (dataObj[i].isChecked)
          selectedData.push(dataObj[i]);
      }
      if (selectedData.length) {
        let customerInfo = {
          pageType: "benefit registrations",
          details: { selectedData, replyToId: this.state.replyToId },
          status: false,
          remark: " test remark",
          sendFor: "reject"
        }
        microsoftTeams.tasks.submitTask(customerInfo, "e0e99be3-0751-4c65-8d14-6c29877f6a2d");
        return true;
      }
    } else {
      selectedData.push(dataObj[index]);
      if (selectedData.length) {
        let customerInfo = {
          pageType: "benefit registrations",
          details: { selectedData, replyToId: this.state.replyToId },
          status: false,
          sendFor: "approve",
          remark: " test remark"
        }
        microsoftTeams.tasks.submitTask(customerInfo, "e0e99be3-0751-4c65-8d14-6c29877f6a2d");
        return true;
      }
    }
  }
  toggle = (index) => {
    const dataObj = this.state.data;
    for (let i in dataObj) {
      if (index == i) {
        continue;
      }
      dataObj[i].isActive = false;
    }
    dataObj[index].isActive = !dataObj[index].isActive;
    this.setState({ data: dataObj });
  }
  selectItem = (index) => {
    const dataObj: any[any] = this.state.data;
    let isChecked
    dataObj[index].isChecked = !dataObj[index].isChecked;
    let dataIndex = dataObj.findIndex(x => x.isChecked === false);
    if (dataIndex === -1) {
      isChecked = true
    }
    if (dataIndex !== -1) {
      isChecked = false
    }
    this.setState({ data: dataObj, isChecked: isChecked });
  }
  render() {
    return <TeamsThemeContext.Consumer>
      {(context) => {
        const { rem, font } = context;
        const { sizes, weights } = font;

        const styles = {
          header: { ...sizes.title, ...weights.semibold },
          section: { ...sizes.title2, marginTop: rem(1.4), marginBottom: rem(1.4) },
          button: { marginRight: rem(0.5) }
        }
        if (this.state.isLoading) {
          //   return <div>Loading...</div>
          return (
            <div className="ms-Spinner"></div>
          )
        }
        else if (this.state.isError) {
          return <div>404</div>
        }
        else {
          return (<div><Table id="table">
            <THead>
              <Tr>
                <Th style={{ flex: "1 0", maxWidth: "fit-content", textAlign: 'left' }}>
                  <IconButton style={styles.button} iconType={MSTeamsIconType.Table} />
                </Th>
                <Th className="p-text" style={{ flex: "1 0", textAlign: 'left' }}>
                  Name
              </Th>
                <Th  className="col-action p-text" style={{ flex: "1 0", textAlign: 'left' }}>
                  Type
              </Th>
                <Th className="col-status p-text" style={{ flex: "1 0", textAlign: 'left' }}>
                  Status
              </Th>
                <Th className="col-action" style={{ flex: "1 0", textAlign: 'right' }}></Th>
              </Tr>
            </THead>
            <TBody >
              {this.state.data && this.state.data.map((item, index) => {
                return (<React.Fragment key={"asda" + index}>
                  <Tr style={item.isActive?{borderBottom:"0px"}:{}}>
                    <Td style={{ flex: "1 0", maxWidth: "fit-content", textAlign: 'left' }}>
                      <IconButton iconType={item.isActive?MSTeamsIconType.ChevronMedDown:MSTeamsIconType.ChevronMedRight} onClick={e => this.toggle(index)} />

                    </Td>
                    <Td className="p-text" style={{ flex: "1 0", textAlign: 'left' }}>
                      {String(item.details.name).length < 30 ? item.details.name : String(item.details.name).slice(0, 15) + "..."}
                    </Td>
                    <Td className="col-action" style={{ flex: "1 0", textAlign: 'left' }}>
                      <IconButton style={styles.button} iconType={item.details.subCategoryId} onClick={e => this.toggle(index)} />
                    </Td>
                    <Td className="col-status p-text" style={{ flex: "1 0", textAlign: 'left' }}>
                      {item.details.actionTaken}
                    </Td>
                    <Td className="col-action" style={{ flex: "1 0", justifyContent: "flex-end", textAlign: 'right', maxWidth: "10% !important" }}>
                      <Checkbox id={"list" + index} checked={item.isChecked} onChecked={e => this.selectItem(index)} />
                    </Td>

                  </Tr>
                  {item.isActive && <Tr>
                    <Td>
                    <Panel style={{ width: "100%" }}>
              <PanelBody style={{padding: "0rem"}}>
                      <Table style={{ minWidth: "100%" }}>
                        <Tr>
                          <Td>Mobile Number</Td>
                          <Td className="col-action-inner-td">{item.details.mobileNumber}</Td>
                        </Tr>
                        <Tr>
                          <Td>Effective From</Td>
                          <Td className="col-action-inner-td">{item.details.effectiveFrom}</Td>
                        </Tr>
                        <Tr>
                          <Td>Effective To</Td>
                          <Td className="col-action-inner-td">{item.details.effectiveTo}</Td>
                        </Tr>
                        <Tr>
                          <Td>Enrollment Amount (p.a.)</Td>
                          <Td className="col-action-inner-td">{item.details.amount}</Td>
                        </Tr>
                      </Table>
                        <Panel style={{width:"100%"}}>
                        <PanelBody style={{padding: "0rem"}} className="footer-button-inner-tr">
                          <PrimaryButton  style={{...styles.button, float:"right"}} onClick={e => this.onApprove(index)}>Approve</PrimaryButton>
                          <SecondaryButton style={{...styles.button, float:"right"}} className="mr-1" onClick={e => this.onReject(index)}>Reject</SecondaryButton>
                        </PanelBody>
                      </Panel>
                      </PanelBody>
                      </Panel>
                    </Td>
                  </Tr>}
                </React.Fragment>
                )
              })}
            </TBody>
          </Table>
            <Panel>
              <PanelBody className="footer-checkbox">
                <Checkbox checked={this.state.isChecked} onChecked={e => this.selectAll(e)} id="selectAll" label="Select All" />
              </PanelBody>
            </Panel>
            <Panel>

              <PanelBody className="footer-button">
                <PrimaryButton style={{...styles.button, float:"right"}} onClick={e => this.onApprove(undefined)}>Approve</PrimaryButton>
                <SecondaryButton style={{...styles.button, float:"right"}} onClick={e => this.onReject(undefined)}>Reject</SecondaryButton>
              </PanelBody>
            </Panel>
          </div>)
        }
      }
      }
    </TeamsThemeContext.Consumer>
  }

  
}

render(
  <GHPages />,
  document.getElementById('root')
);
